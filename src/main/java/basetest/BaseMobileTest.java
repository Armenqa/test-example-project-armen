package basetest;

import Pages.mobile.*;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.PropertiesManager;

public class BaseMobileTest extends BaseTest {
    @Override
    public void setup() {
        super.setup();
        driver.manage().window().setSize(new Dimension(414, 736));
    }


    protected MobileTopPanelPage mobileTopPanel;
    protected MobileAuthPopUpPage mobileAuthPopUp;
    protected MobileCartPage mobileCartPage;
    protected MobileCheckOutPage mobileCheckOutPage;
    protected MobileProductCardPage mobileProductCardPage;
    protected MobileMainPage mobileMainPage;
    protected MobileOspPage mobileOspPage;
    protected MobileSberPage mobileSberPage;
    protected MobileThankForTheOrderPage mobileThankForTheOrderPage;
    protected MobileCookiePage mobileCookiePage;
    protected PropertiesManager propertiesManager = new PropertiesManager();


    @Override
    protected void initPage() {
        mobileTopPanel = new MobileTopPanelPage(driver);
        mobileCheckOutPage = new MobileCheckOutPage(driver);
        mobileCartPage = new MobileCartPage(driver);
        mobileAuthPopUp = new MobileAuthPopUpPage(driver);
        mobileProductCardPage = new MobileProductCardPage(driver);
        mobileMainPage = new MobileMainPage(driver);
        mobileOspPage = new MobileOspPage(driver);
        mobileSberPage = new MobileSberPage(driver);
        mobileCookiePage = new MobileCookiePage(driver);
        mobileThankForTheOrderPage = new MobileThankForTheOrderPage(driver);


    }


}
