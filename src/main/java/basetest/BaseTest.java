package basetest;

import Pages.web.*;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import utils.PropertiesManager;
import utils.WebDriverFactory;

public class BaseTest {
    public WebDriver driver;
    protected TopPanelPage topPanel;
    protected AuthPopUpPage authPopUp;
    protected CartPage cartPage;
    protected CheckOutPage checkOutPage;
    protected ProductCardPage productCardPage;
    protected MainPage mainPage;
    protected OspPage ospPage;
    protected SberPage sberPage;
    protected CookiePage cookiePage;
    protected ThankForTheOrderPage thankForTheOrder;
    private WebDriverFactory driverFactory;
    protected PropertiesManager propertiesManager = new PropertiesManager();
    protected JavascriptExecutor javascriptExecutor;

    private Logger logger = LogManager.getLogger(BaseTest.class);


    protected void initPage() {
        topPanel = new TopPanelPage(driver);
        checkOutPage = new CheckOutPage(driver);
        cartPage = new CartPage(driver);
        authPopUp = new AuthPopUpPage(driver);
        productCardPage = new ProductCardPage(driver);
        mainPage = new MainPage(driver);
        ospPage = new OspPage(driver);
        sberPage = new SberPage(driver);
        cookiePage = new CookiePage(driver);
        thankForTheOrder = new ThankForTheOrderPage(driver);
    }
    @Step("Пользователь переходит в карточку товара")
    protected void openUrl(String url) {
        driver.get(url);
        saveAllureScreenshot();
    }

    @Before
    @Step("Открывается Главная страница сайта Горздрав")
    public void setup() {
        driverFactory = new WebDriverFactory();
        driver = driverFactory.getDriver();
        initPage();
        driver.get(propertiesManager.getProperty("baseurl"));
        saveAllureScreenshot();
        logger.info("Открывается главная страница сайта Горздрав");

    }

    @After
    public void finish() {

    }

    @FindBy(xpath = "//div[contains(text(), 'Препараты по алфавиту')]")
    private WebElement ospTitle;

    @Step("Пользователь обновляет страницу")
    public void reloadPage() {
        logger.info("ПОЛЬЗОВАТЕЛЬ ОБНОВЛЯЕТ СТРАНИЦУ");
        driver.navigate().refresh();
        logger.info("СТРАНИЦА ОБНОВЛЕНА");
        saveAllureScreenshot();

    }
    @Step("Пользователь скроллит страницу до 'Препараты по алфавиту'")
    public void ScrollToOsp() {
        javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", ospTitle);
        logger.info("СТРАНИЦА ПРОСКРОЛЛЕНА ДО ВЫБРАННОГО ЭЛЕМЕНТА");
        saveAllureScreenshot();
    }
    @Step("Пользователь скроллит страницу вниз на N px")
    public void ScrollToObject() {
        javascriptExecutor.executeScript("window.scrollBy(0, 4200)");
        logger.info("СТРАНИЦА ПРОСКРОЛЛЕНА ВНИЗ НА N ПИКСЕЛЕЙ");
        saveAllureScreenshot();
    }
    @Step("Пользователь скроллит страницу вниз до конца")
    public void ScrollToBottom() {
        javascriptExecutor.executeScript("window.scrollBy(0, document.body.scrollHeight)");
        logger.info("СТРАНИЦА ПРОСКРОЛЛЕНА ВНИЗ ДО КОНЦА");
        saveAllureScreenshot();
    }


    /**
     * Повторный запуск тестов при падении
     */
    @Rule
     public RetryRule rule = new RetryRule(3);

    /**
     * Управление действиями, при различных исходах теста
     */
    @Rule
    public TestWatcher watchman = new TestWatcher() {
        @Override
        protected void failed(Throwable e, Description description) {
            logger.info("Тест упал");
        }

        @Override
        protected void succeeded(Description description) {
            logger.info("Тест успешно завершен");
        }

        @Override
        protected void finished(Description description) {
            driver.quit();
        }

        @Override
        protected void starting(Description description) {
            logger.info("Тест старт");
        }
    };

    /**
     * @return - скриншот
     */
    @Attachment(value = "Скриншот", type = "image/png")
    public byte[] saveAllureScreenshot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    /**
     * Прикрепить строку к отчету
     *
     * @param string - Входящая строка
     * @return - строка к отчету
     */
    @Attachment(value = "строка")
    public String returnString(String string) {
        return string;
    }

    @Attachment(value = "Result")
    public String returnAttachment() {
        return "Все хорошо";
    }

    public void waitLoader() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
