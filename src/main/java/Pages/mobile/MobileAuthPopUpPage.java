package Pages.mobile;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class MobileAuthPopUpPage extends BaseMobilePage {
    public MobileAuthPopUpPage(WebDriver driver){
        super(driver);
    }

    @FindBy(css = ".b-link--auth.js-auth-ext__button")
    private WebElement authTypeButton;
    @FindBy(id = "login-phone")
    private WebElement phoneNumberInput;
    @FindBy(id = "login-pass")
    private WebElement passwordInput;
    @FindBy(css = ".js-gtm-upload-login-form")
    private WebElement loginButton;
    @FindBy(xpath = "//*[contains (@class, 'b-simple-tab__wrapper')]//a[contains (@class, 'b-simple-tab')][2]")
    private WebElement changeToCardNumber;
    @FindBy(id = "login-cardNumber")
    private WebElement authCardNumber;
    @FindBy(xpath = "//*[contains (@class, 'b-simple-tab__wrapper')]//a[contains (@class, 'b-simple-tab')][3]")
    private WebElement authEmail;
    @FindBy(id = "login-email")
    private WebElement inputEmail;



    @Step("переключение на вход по паролю")
    public void clickChangeAuth(){
        authTypeButton.click();
    }
    @Step("ввод номера телефона - {phoneNumber}")
    public void setPhoneNumber(String userauthphone){
        phoneNumberInput.sendKeys(userauthphone);
    }
    @Step("ввод пароля - {password}")
    public void setPassword(String userpass){
        passwordInput.sendKeys(userpass);
    }
    @Step("клик на кнопку \"войти\"")
    public void clickToLoginButton(){
        loginButton.click();
    }

    @Step("Пользователь переключается на вход по номеру бонусной карты")
    public void setChangeToCardNumber(){
        changeToCardNumber.click();
    }
    @Step("Пользователь вводит номер бонусной карты")
    public void inputAuthCardNumber(String userauthcard){
        authCardNumber.sendKeys(userauthcard);
    }
    @Step("Пользователь переключается на вкладку авторизации по E-mail")
    public void changeToEmailAuth(){
        authEmail.click();
    }
    @Step("Пользователь вводит E-mail")
    public void inputEmail(String userauthmail){
        inputEmail.sendKeys(userauthmail);
    }
}
