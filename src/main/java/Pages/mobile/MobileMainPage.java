package Pages.mobile;

import Pages.web.MainPage;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MobileMainPage extends MainPage {
    public MobileMainPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//input[@class='b-search__input']")
    private WebElement clickMobileSearch;
    @FindBy(xpath = "//input[@id='js-site-search-input']")
    private WebElement mobileSearchInput;

    @Step("Пользователь кликает по поисковому окну и вводит артикул товара")
    @Override
    public void setSearchInput(String search) {
        clickMobileSearch.click();
       mobileSearchInput.sendKeys(search);
       saveAllureScreenshot();
    }

    @Step("Пользователь вводит артикул товара в поисковую строку")
    public void setMobileSearchInput(String search){
        mobileSearchInput.sendKeys(search);
        saveAllureScreenshot();
    }



}
