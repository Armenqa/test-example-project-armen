package Pages.mobile;

import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class MobileOspPage extends BaseMobilePage {
    private Logger logger = LogManager.getLogger(MobileOspPage.class);
    public MobileOspPage(WebDriver driver) {
        super(driver);
    }
    //web-элементы

    @FindBy(xpath = "//*[@href=\"/catalog/nurofen\"]")
    private WebElement productName;
    @FindBy(xpath = "//*[@class=\"c-prod-item__buttons\"]//button[contains(@class,\"js-add-to-cart\")]")
    private List<WebElement> kupitButton;
    @FindBy(xpath = "//*[@href='#analogs']")
    private WebElement analogs;
    @FindBy(xpath = "//span[contains (@class, 'c-alphabet-list__show-all js-show--all')]")
    private WebElement shawAll;
    @FindBy(xpath = "(//*[contains (@class, 'b-btn-merger--right js-add-to-cart')])[1]")
    private WebElement clickFirstKupitButton;

    // действия

    @Step("Пользователь нажимает на кнопку 'Показать еще'")
    public void clickToShawAll(){
        shawAll.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимает на кнопку 'Купить' напротив первого товара")
    public void clickFirst(){
        clickFirstKupitButton.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь скроллит страницу вниз на N px")
    public void ScrollToObject() {
        javascriptExecutor.executeScript("window.scrollBy(0, 200)");
        logger.info("СТРАНИЦА ПРОСКРОЛЛЕНА ВНИЗ НА N ПИКСЕЛЕЙ");
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимает на название препарата")
    public void ClickOnProductName() {
        productName.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимает на {number} - ую кнопку 'Купить' напротив выбранного препарата")
    public void clickOnKupitButton(int number) {
        kupitButton.get(number).click();
        saveAllureScreenshot();
    }

    @Step("Пользователь пеерходит на вкладку 'Аналоги'")
    public void clickToAnalogs() {
        analogs.click();
        saveAllureScreenshot();
    }

}
