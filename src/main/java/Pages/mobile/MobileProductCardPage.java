package Pages.mobile;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class MobileProductCardPage extends BaseMobilePage {
    public MobileProductCardPage(WebDriver driver) {
        super(driver);
    }
    // web-элементы

    @FindBy(css = ".js-order-quick-navigate")
    private WebElement oneClick;
    @FindBy(xpath = "//*[@class='b-search js-store-finder-search__form']//*[contains(@class, 'b-search__buttons')]")
    private WebElement lupaButtonCard;
    @FindBy(className = "js-order-quick__button")
    private List<WebElement> oneClickAptekaButtons;
    @FindBy(css = ".js-storefinder-recommend")
    private WebElement changeAptekaList;
    @FindBy(xpath = "//*[@data-price-id='21']")
    private WebElement discountPrice;
    @FindBy(css = ".js-price-value")
    private WebElement priceLable;
    @FindBy(css = ".js-add-to-cart--quick")
    private WebElement addToCartButton;
    @FindBy(xpath = "//*[contains (@class, 'c-tabs-inline js-product-stores__head-mobile')]//*[contains (@class, 'c-tabs-inline__item js-product-stores__head-item')][1]")
    private WebElement clickList;
    @FindBy(css = ".js-product-discount-header")
    private WebElement profitableOffer;
    @FindBy(css = ".js-product-stores__head-mobile")
    private WebElement productStores;

    // действия

    @Step("Пользователь скроллит страницу вниз на N px")
    public void ScrollToObject() {
        javascriptExecutor.executeScript("window.scrollBy(0, 4200)");
//        logger.info("СТРАНИЦА ПРОСКРОЛЛЕНА ВНИЗ НА N ПИКСЕЛЕЙ");
        saveAllureScreenshot();
    }
    @Step("Пользователь скроллит страницу вниз на N px")
    public void scrollDown() {
        javascriptExecutor.executeScript("window.scrollBy(0, 200)");
//        logger.info("СТРАНИЦА ПРОСКРОЛЛЕНА ВНИЗ НА N ПИКСЕЛЕЙ");
        saveAllureScreenshot();
    }

    @Step("Пользователь скроллит страницу до выбора аптек 'На карте/Списком'")
    public void scrollToProductStores(){
        javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", productStores);
        saveAllureScreenshot();
    }

    @Step("Пользователь переключается на отображение аптек списком")
    public void clickList(){
        clickList.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимает на кнопку 'Купить в 1 клик' ")
    public void clickToOneClick() {
        oneClick.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимает на иконку лупы")
    public void clickLupaButtonCard() {
        lupaButtonCard.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимает на {number} - ую кнопку 'Купить в 1 клик' напротив выбранной аптеки")
    public void clickOneClickApteka(int number) {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(oneClickAptekaButtons.get(number)));
        oneClickAptekaButtons.get(number).click();
        saveAllureScreenshot();
    }

    @Step("Пользователь переключается на отображение аптек списком")
    public void changeAptekaList() {
        changeAptekaList.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь выбирает дисконтную цену")
    public void clickDiscountPrice(){
        discountPrice.click();
        saveAllureScreenshot();
    }

    @Step("Запоминаем цену товара")
    public int getProductPrice(){
        String stringPrice = priceLable.getText(); // Создали переменнут типа String которая в виде строки забирает цену
        int price = Integer.parseInt(stringPrice); // Метод позволяет переводить строку в число
        return price; // вернули число
    }

    @Step("Пользователь нажимает на кнопку 'Купить' в карточке товара")
    public void clickAddToCartButton() throws InterruptedException {
        Thread.sleep(3000);
        addToCartButton.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь скроллит страницу до плашки 'Выгодное предложение'" )
    public void scrollToProfitableOffer(){
        javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", profitableOffer);
        saveAllureScreenshot();
    }

}
