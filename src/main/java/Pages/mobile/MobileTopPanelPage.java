package Pages.mobile;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MobileTopPanelPage extends BaseMobilePage {
    public MobileTopPanelPage(WebDriver driver){
        super(driver);
    }

    // web-элементы

    @FindBy(css = ".js-to-menu__item--1")
    private WebElement loginIcon;
    @FindBy(xpath = "//*[contains(@class, \"js-dropdown-menu-button\")]")
    private WebElement burgerButton;

    // действия

    @Step("Нажимаем на иконку авторизации")
    public void clickToLoginIcon(){
        loginIcon.click();
        saveAllureScreenshot();
    }
    @Step("Пользователь нажимает на иконку каталога")
    public void clickToBurgerButton(){
        burgerButton.click();
        saveAllureScreenshot();
    }

}
