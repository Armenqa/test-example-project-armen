package Pages.mobile;

import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MobileSberPage extends BaseMobilePage {
    private Logger logger = LogManager.getLogger(MobileSberPage.class);
    public MobileSberPage(WebDriver driver) {
        super(driver);
    }

    // web-элементы

    @FindBy(xpath = "//input[@id='pan']")
    private WebElement cardNumber;
    @FindBy(xpath = "//input[@id='expiry']")
    private WebElement monthYear;
    @FindBy(xpath = "//input[@id='cvc']")
    private WebElement cvv;
    @FindBy(xpath = "//*[@class='styles_button__1M9-J styles_solid__1fLFs']")
    private WebElement submitButton;

    // действия

    @Step("Пользователь вводит номер пластиковой карты")
    public void inputCardNumber(String number){
        cardNumber.sendKeys(number);
        saveAllureScreenshot();
    }
    @Step("Пользователь вводит месяц/год срока службы пластиковой карты")
    public void inputMonthYear(String number){
        monthYear.sendKeys(number);
        saveAllureScreenshot();
    }
    @Step("Пользователь вводит cvv-код")
    public void inputCvv(String number){
        cvv.sendKeys(number);
        saveAllureScreenshot();
        logger.info("ПОЛЬЗОВАТЕЛЬ ВВЕЛ ДАННЫЕ КАРТЫЕ ДЛЯ ОПЛАТЫ");
    }
    @Step("Пользователь нажимает на кнопку 'Оплатить'")
    public void clickOnSubmitButton(){
        submitButton.click();
        saveAllureScreenshot();
        logger.info("ПОЛЬЗОВАТЕЛЬ НАЖАЛ НА КНОПКУ 'ОПЛАТИТЬ'");
    }
}
