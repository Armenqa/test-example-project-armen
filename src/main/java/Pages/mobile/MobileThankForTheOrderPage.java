package Pages.mobile;

import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MobileThankForTheOrderPage extends BaseMobilePage {

    Logger logger = LogManager.getLogger(MobileThankForTheOrderPage.class);


    public MobileThankForTheOrderPage(WebDriver driver) {
        super(driver);
    }

    // web-элементы
    @FindBy(xpath = "//*[contains(text(), 'Спасибо за ваш заказ!')]")
    private WebElement ThanksOrder;

    @FindBy(xpath = "//*[contains(text(), 'Экспресс-доставка:')]")
    private WebElement ThanksOrderDelivery;

    @FindBy(xpath = "//*[contains(text(), 'Спасибо, вы оформили 2 заказа')]")
    private WebElement thanksOrderSplit;


    // действия

    @Step("Пользователь проверяет отображение номера заказа оформленного самовывозом")
    public void checkOrderNumberSelf(){
        Assert.assertTrue(ThanksOrder.isDisplayed());
        saveAllureScreenshot();
        logger.info("Пользователь получает номер заказа");

    }

    @Step("Пользователь проверяет отображение номера заказа оформленного самовывозом")
    public void checkOrderNumberDelivery(){
        Assert.assertTrue(ThanksOrderDelivery.isDisplayed());
        saveAllureScreenshot();
        logger.info("Пользователь получает номер заказа");
    }

    @Step("Пользователь проверяет отображение номеров Сплит-заказа ")
    public void checkOrderNumberSplit(){
        Assert.assertTrue(thanksOrderSplit.isDisplayed());
        saveAllureScreenshot();
        logger.info("Пользователь получает номер заказа");
    }
    @Step("Пользователь скроллит страницу вниз на N px")
    public void ScrollToObject() {
        javascriptExecutor.executeScript("window.scrollBy(0, 280)");
        logger.info("СТРАНИЦА ПРОСКРОЛЛЕНА ВНИЗ НА N ПИКСЕЛЕЙ");
        saveAllureScreenshot();
    }
}
