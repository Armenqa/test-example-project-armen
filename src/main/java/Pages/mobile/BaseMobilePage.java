package Pages.mobile;

import Pages.web.BasePage;
import io.qameta.allure.Attachment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

public class BaseMobilePage extends BasePage {

    public BaseMobilePage(WebDriver driver) {
        super(driver);
    }
}
