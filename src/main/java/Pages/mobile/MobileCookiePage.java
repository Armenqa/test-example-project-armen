package Pages.mobile;

import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import java.util.HashSet;
import java.util.Set;

public class MobileCookiePage extends BaseMobilePage {
     private Logger logger = LogManager.getLogger(MobileCookiePage.class);

    public MobileCookiePage(WebDriver driver) {
        super(driver);
    }
    @Step("Пользователь авторизовывается через Cookie")
    public void cookieAuthorization() {
        logger.info("ПРОИЗОШЛА АВТОРИЗАЦИЯ ПОЛЬЗОВАТЕЛЯ ЧЕРЕЗ COOKIE");
        //stg1
        driver.manage().addCookie(new Cookie("acceleratorSecureGUID", "9b05b8068651765e8437cc2a75de9dc37589c1c7"));
        driver.manage().addCookie(new Cookie("avestorefrontRememberMe", "W2dvcnpkcmF2XTk0MDMzOTQ1NTU6MTYzMTI3MzMxMDMzMTo5OGRmMTViMDUzMWM2MTM5OGFjYzFhMTU2MzI4MTlhMQ"));
        driver.manage().addCookie(new Cookie("JSESSIONID", "B1149196AB51CDBE16C43E6A13E07A76.stg-hybris-app-03"));

        // D1
//        driver.manage().addCookie(new Cookie("acceleratorSecureGUID", "332091cf3bcec54cf1a586a85ac2aeac70a285d4"));
//        driver.manage().addCookie(new Cookie("avestorefrontRememberMe", "W2dvcnpkcmF2XTk0MDc4Nzg1NTM6MTYzMTcwOTExNjY5MzpmZDVkZWU2MzJjNmU0YmVkMzgzN2U3NWEwZDFmYmI3MA"));
//        driver.manage().addCookie(new Cookie("ROUTE", ".accstorefront-595bfb867d-ppvx8"));

//        driver.manage().addCookie(new Cookie("acceleratorSecureGUID", "23b3b99c12da43c02b8967f11d0f9dff2516fcbe")); //кука для обхода каптчи
        // S1
//        driver.manage().addCookie(new Cookie("acceleratorSecureGUID", "5dc4a06aa1f9c900769b9280ba9ca5593cf8d5ba"));
//        driver.manage().addCookie(new Cookie("avestorefrontRememberMe", "W2dvcnpkcmF2XTk0MDc4Nzg1NTU6MTYzMTI2MTg0MzU1Nzo5YmY0NjU2NDM0OTdiNzFkZTBhMjhiOGIzNzE0MDI4MA"));
//        driver.manage().addCookie(new Cookie("ROUTE", ".accstorefront-5bd47ccb5c-59d2d"));
//        print();
    }
    public void reCaptchaKey(){
        driver.manage().addCookie(new Cookie("acceleratorSecureGUID", "23b3b99c12da43c02b8967f11d0f9dff2516fcbe"));
    }

    @Step("Получение всех кук и вывод их в консоль")
    public void printAllCookie() {
        Set<Cookie> cookies = new HashSet<>();
        cookies = driver.manage().getCookies();
        for (Cookie cookie : cookies) {
            String cookieName = cookie.getName();
            System.out.println("Имя куки - " + cookieName + " значение  куки - " + cookie.getValue());
        }
    }

    @Step("Пользователь очищает Cookie")
    public void deleteAllCookie() {
        driver.manage().deleteAllCookies();
        logger.info("ПОЛЬЗОВАТЕЛЬ ОЧИСТИЛ COOKIE");
    }


}
