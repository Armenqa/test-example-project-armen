package Pages.web;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CheckOutPage extends BasePage {

    public CheckOutPage(WebDriver driver) {
        super(driver);
    }


    @FindBy(css = ".js-checkout--mixed__shipment-delivery")
    private WebElement chooseDeliveryMethod;
    @FindBy(xpath = "//*[@id='address']")
    private WebElement inputAddress;
    @FindBy(xpath = "//*[@id=\"checkout--suggestions_address\"]/div[2]/div[1]")
    private WebElement dropDownMenu;
    @FindBy(xpath = "//*[@id='address-flat']")
    private WebElement inputFlatNumber;
    @FindBy(xpath = "//*[@id='phone']")
    private WebElement inputPhoneNumber;
    @FindBy(css = ".js-checkout--mixed__contacts")
    private WebElement finalBuyButton;
    @FindBy(xpath = "//*[contains(@class, 'js-address-search-input')]")
    private WebElement inputSearchAddres;
    @FindBy(xpath = "//button[contains(@class, 'b-search__button')]")
    private WebElement lupaButton;
    @FindBy(xpath = "//span[contains(@class, 'p-checkout--mixed__control-labelText ')]")
    private WebElement changeAptekaList;
    @FindBy(xpath = "//input[@id='phone']")
    private WebElement inputMyPhoneNumber;
    @FindBy(xpath = "//input[@id='fio']")
    private WebElement inputMyName;
    @FindBy(xpath = "//input[@id='email']")
    private WebElement inputMyEmail;
    @FindBy(css = ".js-checkout--mixed__contacts")
    private WebElement oformitZakazButton;
    @FindBy(xpath = "//input[contains(@class, 'b-input--text')]")
    private WebElement inputOneClickPhoneNumber;
    @FindBy(xpath = "//input[contains(@class, 'js-submit-quick')]")
    private WebElement zabronirovatButton;
    @FindBy(css = ".js-store-select:first-child")
    private WebElement getAvailability;
    @FindBy(css = ".js-store-select:first-child")
    private WebElement chooseThisPharmacy;


    @Step("Пользователь нажимает на кнопку Оформить заказ")
    public void setOformitZakazButton() {
        oformitZakazButton.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь вводит емайл")
    public void inputMyEmail(String myEmail) {
        inputMyEmail.sendKeys(myEmail);
        saveAllureScreenshot();
    }

    @Step("Пользователь вводит имя")
    public void inputMyName(String myName) {
        inputMyName.sendKeys(myName);
        saveAllureScreenshot();
    }

    @Step("Пользователь вводит номер телефона")
    public void inputMyPhoneNumber(String phoneNumber) {
        inputMyPhoneNumber.sendKeys("9283394503");
        saveAllureScreenshot();
    }



    @Step("Пользователь нажимает на кнопку 'Узнать о наличии'")
    public void getAvailabilityAndChooseThisPharmacy(){
        getAvailability.click();
        chooseThisPharmacy.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимает на кнопку 'Выбрать эту аптеку'")
    public void chooseThisPharmacy(){
        chooseThisPharmacy.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь вводит номер телефона при покупке в 1-клик")
    public void setInputOneClickPhoneNumber(String number) {
        inputOneClickPhoneNumber.sendKeys(number);
        saveAllureScreenshot();
    }

    @Step("Пользователь переключается на отображение аптек Списком")
    public void chengeAptekaList() {
        changeAptekaList.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимает на иконку лупы")
    public void lupaButton() {
        lupaButton.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь вводит адрес - {addres}")
    public void setInputSearchAddres(String addres) {
        inputSearchAddres.sendKeys(addres);
        lupaButton.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь выбирает способ получения - Доставка")
    public void clickDeliveryMethod() {
        chooseDeliveryMethod.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь водит адрес доставки на чекауте - {address}")
    public void setInputAddress(String address) {
        inputAddress.sendKeys(address);
        saveAllureScreenshot();
    }

    @Step("Пользователь кликает по выпадающему меню")
    public void clickToDropDownMenu() {
        dropDownMenu.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь водит номер квартиры - {number}")
    public void setFlatNumber(String number) {
        inputFlatNumber.sendKeys(number);
        saveAllureScreenshot();
    }

    @Step("Пользователь вводит номер телефона - {phone}")
    public void setInputPhoneNumber(String phone) {
        inputPhoneNumber.sendKeys(phone);
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимает на кнопку купить")
    public void clickToFinalButton() {
        finalBuyButton.click();
        logger.info("ПОЛЬЗОВАТЕЛЬ ПЕРЕШЕЛ НА СТРАНИЦУ СБЕРА");
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимает на кнопку 'Забронировать'")
    public void clickOnZabronirivatButton(){
        zabronirovatButton.click();
        saveAllureScreenshot();
    }

}
