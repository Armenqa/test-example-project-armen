package Pages.web;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends BasePage {
    public MainPage(WebDriver driver) {
        super(driver);
    }

    // web-элементы
    @FindBy(xpath = "//*[@href=\"/littera-Н/\"]")
    private WebElement letterN;
    @FindBy(xpath = "//*[@id='component-SiteLogoComponent']")
    private WebElement siteLogo;
    @FindBy(xpath = "//input[@id='js-site-search-input']")
    private WebElement searchInput;

    // действия

    @Step("Пользователь вводит артикул товара в поисковую строку - {search}")
    public void setSearchInput(String search) {
        searchInput.sendKeys(search);
        logger.info("Пользователь вводит артикул не партнерсского товара в поисковую строку");
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимает на русскую букву 'Н'")
    public void ClickOnLetter() {
        letterN.click();
        logger.info("Пользователь нажимает на русскую букву 'Н'");
        saveAllureScreenshot();
    }
    @Step("Проверка отображения логотипа сайта на главной странице")
    public void checkElementIsCorrect(){
        siteLogo.isDisplayed();
        logger.info("Лого отображается");
        saveAllureScreenshot();
    }


}
