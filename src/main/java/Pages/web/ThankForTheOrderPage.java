package Pages.web;

import io.qameta.allure.Step;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ThankForTheOrderPage extends BasePage {


    public ThankForTheOrderPage(WebDriver driver) {
        super(driver);
    }

    // web-элементы
    @FindBy(xpath = "//*[contains(text(), 'Спасибо за ваш заказ!')]")
    private WebElement thanksOrderSelf;

    @FindBy(xpath = "//*[contains(text(), 'Экспресс-доставка:')]")
    private WebElement thanksOrderDelivery;

    @FindBy(xpath = "//*[contains(text(), 'Спасибо, вы оформили 2 заказа')]")
    private WebElement thanksOrderSplit;


    // действия

    @Step("Пользователь проверяет отображение номера заказа оформленного самовывозом")
    public void checkOrderNumberSelf(){
        Assert.assertTrue(thanksOrderSelf.isDisplayed());
        logger.info("Пользователь получает номер заказа");
        saveAllureScreenshot();

    }

    @Step("Пользователь проверяет отображение номера заказа оформленного доставкой")
    public void checkOrderNumberDelivery(){
        Assert.assertTrue(thanksOrderDelivery.isDisplayed());
        logger.info("Пользователь получает номер заказа");
        saveAllureScreenshot();
    }

    @Step("Пользователь проверяет отображение номеров Сплит-заказа ")
    public void checkOrderNumberSplit(){
        ScrollToObject();
        Assert.assertTrue(thanksOrderSplit.isDisplayed());
    }

    @Step("Пользователь скроллит страницу вниз на N px")
    public void ScrollToObject() {
        javascriptExecutor.executeScript("window.scrollBy(0, 290)");
        logger.info("СТРАНИЦА ПРОСКРОЛЛЕНА ВНИЗ НА N ПИКСЕЛЕЙ");
        saveAllureScreenshot();
    }
}
