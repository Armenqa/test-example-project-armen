package Pages.web;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

public class BasePage {
    Logger logger = LogManager.getLogger(BasePage.class);

    protected WebDriverWait webDriverWait;
    protected WebDriver driver;
    protected JavascriptExecutor javascriptExecutor;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        webDriverWait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver, this);
        javascriptExecutor = (JavascriptExecutor) driver;
    }

    public void waitVisibility(By elementBy) {
        webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(elementBy));
    }

    public void isElementDisplayed(By elementBy) {
        waitVisibility(elementBy);
        assertTrue(driver.findElement(elementBy).isDisplayed());
    }

    public void click(By elementBy) {
        waitVisibility(elementBy);
        driver.findElement(elementBy).click();
    }



    /**
     *
     * @return - скриншот
     */
    @Attachment(value = "Скриншот", type = "image/png")
    public byte [] saveAllureScreenshot(){
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }


    /*
    Метод кликает на элемент до 5-ти раз
     */
//    protected void retryClick(By by){
//        int i = 0; // переменная счетчик кликов
//        while (i < 5){
//            try {
//                driver.findElement(by).click();
//                break;
//            }
//            catch (StaleElementReferenceException e){
//                i++;
//            }
//        }
//
//    }
}
