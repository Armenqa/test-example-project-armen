package Pages.web;

import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.PropertiesManager;

public class CartPage extends BasePage {
    private Logger logger = LogManager.getLogger(CartPage.class);
    private PropertiesManager propertiesManager = new PropertiesManager();


    public CartPage(WebDriver driver) {
        super(driver);
    }

    // web-элементы

    @FindBy(xpath = "//button[contains(@class, \"js-add-to-cart\")]")
    private WebElement buyButton;
    @FindBy(css = ".c-mini-cart__icon")
    private WebElement cartButton;
    @FindBy(xpath = "//*[@href='/cart/checkout']")
    private WebElement oformitZakaz;
    @FindBy(xpath = "//*[@class='c-prod-item__thumb']")
    private WebElement productCard;
    @FindBy(css = ".pull-right js-revenue")
    private WebElement totalSum;
    @FindBy(xpath = "//h1[contains(text(), 'Ваша корзина пуста')]")
    private WebElement emptyCartText;
    @FindBy(css = ".c-cart-summary__total span b")
    private WebElement totalPrice;
    @FindBy(css = ".js-cart-clear")
    private WebElement clearAllFromCart;
    @FindBy(css = ".js-cart-clear-yes")
    private WebElement confirmCleanAll;
    @FindBy(css = ".js-mini-cart-count")
    private WebElement cartCount;

    // действия

    @Step("Пользователь нажимает на кнопку 'Да, подтверждаю'")
    public void confirmCleanAll() {
        confirmCleanAll.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь переходит в карточку товара")
    public void goToProductCard() {
        productCard.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимает на кнопку 'Очистить все в корзине'")
    public void clearAllFromCart() {
        clearAllFromCart.click();
        confirmCleanAll.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимает на кнопку 'купить'")
    public void clickBuyButton() {
        buyButton.click();
        logger.info("Пользователь нажимает на кнопку 'купить'");
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимает на иконку корзины")
    public void clickToCartButton() {
        cartButton.click();
        logger.info("Пользователь нажимает на иконку корзины");
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимае на кнопку 'Оформить заказ'")
    public void clickToOformitZakaz() {
        oformitZakaz.click();
        logger.info("Пользователь нажимае на кнопку 'Оформить заказ'");
        saveAllureScreenshot();
    }

    @Step("Проверка на отображение текста 'Ваша корзина пуста'")
    public void assertEmptyCart() {
        driver.get(propertiesManager.getProperty("baseurl") + "/cart");
        Assert.assertTrue(emptyCartText.isDisplayed());
        logger.info("КОРЗИНА ПУСТА - ОК");
        saveAllureScreenshot();

    }

    @Step("Сохранение итоговой суммы в корзине")
    public int getTotalPrice() throws InterruptedException {
        Thread.sleep(3000);
        String stringTotalPrice = totalPrice.getText().replaceAll("[^0-9]", "").trim();
        return Integer.parseInt(stringTotalPrice);

    }


    @Step("Проверка состояния корзины: Если корзина не пустая, удаляем все содержимое")
    public void checkCartQuantity(){
        logger.info("ОЧИСТКА КОРЗИНЫ, ЕСЛИ В НЕЙ ЕСТЬ ТОВАРЫ");
        String stringCartQuantity = cartCount.getText();
        int quantity = Integer.parseInt(stringCartQuantity);
        if(quantity != 0){
            logger.info("В КОРЗИНЕ ЕСТЬ ТОВАРЫ");
            driver.get(propertiesManager.getProperty("baseurl") + "/cart");
            clearAllFromCart();
            logger.info("ТОВАРЫ В КОРЗИНЕ УДАЛЕНЫ");
            saveAllureScreenshot();
        }
    }

}
