package Pages.web;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class OspPage extends BasePage {
    public OspPage(WebDriver driver) {
        super(driver);
    }
    //web-элементы

    @FindBy(xpath = "//*[@href=\"/catalog/nurofen\"]")
    private WebElement productName;
    @FindBy(xpath = "//*[@class=\"c-prod-item__buttons\"]//button[contains(@class,\"js-add-to-cart\")]")
    private List<WebElement> kupitButton;
    @FindBy(xpath = "//*[@href='#analogs']")
    private WebElement analogs;

    // действия

    @Step("Пользователь нажимает на название препарата")
    public void ClickOnProductName() {
        productName.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимает на {number} - ую кнопку 'Купить' напротив выбранного препарата")
    public void clickOnKupitButton(int number) {
        kupitButton.get(number).click();
        saveAllureScreenshot();
    }

    @Step("Пользователь пеерходит на вкладку 'Аналоги'")
    public void clickToAnalogs() {
        analogs.click();
        saveAllureScreenshot();
    }

}
