package Pages.web;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class ProductCardPage extends BasePage {
    public ProductCardPage(WebDriver driver) {
        super(driver);
    }
    // web-элементы

    @FindBy(css = ".js-order-quick-navigate")
    private WebElement oneClick;
    @FindBy(xpath = "//*[@class='b-search js-store-finder-search__form']//*[contains(@class, 'b-search__buttons')]")
    private WebElement lupaButtonCard;
    @FindBy(className = "js-order-quick__button")
    private List<WebElement> oneClickAptekaButtons;
    @FindBy(css = ".js-storefinder-recommend")
    private WebElement changeAptekaList;
    @FindBy(xpath = "//*[@data-price-id=\"21\"]")
    private WebElement discountPrice;
    @FindBy(css = ".js-price-value")
    private WebElement priceLable;
    @FindBy(css = ".js-add-to-cart--quick")
    private WebElement addToCartButton;
    @FindBy(css = ".js-storefinder-sort")
    private WebElement changeAptekaListDiscount;

    // действия

    @Step("Пользователь нажимает на кнопку 'Купить в 1 клик' ")
    public void clickToOneClick() {
        oneClick.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимает на иконку лупы")
    public void clickLupaButtonCard() {
        lupaButtonCard.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь нажимает на {number} - ую кнопку 'Купить в 1 клик' напротив выбранной аптеки")
    public void clickOneClickApteka(int number) {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(oneClickAptekaButtons.get(number)));
        oneClickAptekaButtons.get(number).click();
        saveAllureScreenshot();
    }

    @Step("Пользователь переключается на отображение аптек списком")
    public void changeAptekaList() {
        changeAptekaList.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь переключается на отображение аптек списком")
    public void changeAptekaListDiscount(){
        changeAptekaListDiscount.click();
        saveAllureScreenshot();
    }

    @Step("Пользователь выбирает дисконтную цену")
    public void clickDiscountPrice(){
        discountPrice.click();
        saveAllureScreenshot();
    }

    @Step("Запоминаем цену товара")
    public int getProductPrice(){
        String stringPrice = priceLable.getText(); // Создали переменнут типа String которая в виде строки забирает цену
        int price = Integer.parseInt(stringPrice); // Метод позволяет переводить строку в число
        return price; // вернули число
    }

    @Step("Пользователь нажимает на кнопку 'Купить' в карточке товара")
    public void clickAddToCartButton() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        addToCartButton.click();
        saveAllureScreenshot();
    }

}
