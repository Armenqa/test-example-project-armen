package Pages.web;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SberPage extends BasePage {
    public SberPage(WebDriver driver) {
        super(driver);
    }

    // web-элементы

    @FindBy(xpath = "//input[@id='pan']")
    private WebElement cardNumber;
    @FindBy(xpath = "//input[@id='expiry']")
    private WebElement monthYear;
    @FindBy(xpath = "//input[@id='cvc']")
    private WebElement cvv;
    @FindBy(xpath = "//*[@class='styles_button__1M9-J styles_solid__1fLFs']")
    private WebElement submitButton;

    // действия

    @Step("Пользователь вводит номер пластиковой карты")
    public void inputCardNumber(String number){
        cardNumber.sendKeys(number);
        saveAllureScreenshot();
    }
    @Step("Пользователь вводит месяц/год срока службы пластиковой карты")
    public void inputMonthYear(String number){
        monthYear.sendKeys(number);
        saveAllureScreenshot();
    }
    @Step("Пользователь вводит cvv-код")
    public void inputCvv(String number){
        cvv.sendKeys(number);
        saveAllureScreenshot();
    }
    @Step("Пользователь нажимает на кнопку 'Оплатить'")
    public void clickOnSubmitButton(){
        submitButton.click();
        saveAllureScreenshot();
    }
}
