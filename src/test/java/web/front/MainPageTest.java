package web.front;

import Pages.web.MainPage;
import basetest.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

@Feature(value = "Web")
@Story("Проверка главной страницы сайта")
@DisplayName("Проверка главной страницы")
public class MainPageTest extends BaseTest {
    private Logger logger = LogManager.getLogger(MainPage.class);
    @DisplayName("Проверка главной страницы сайта")
    @Test
    public void checkMainPage(){
        mainPage.checkElementIsCorrect();
        logger.info("Лого отображается на сайте");
        cartPage.assertEmptyCart();
        saveAllureScreenshot();
    }




}
