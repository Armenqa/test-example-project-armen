package web.makeorder.unauthorized;

import basetest.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@Feature(value = "Web")
@Story(value = "Покупка в 1 клик")
@DisplayName("Оформление основного товара в 1 клик")
public class OneClickPurchaseTest extends BaseTest {
    private Logger logger = LogManager.getLogger(OneClickPurchaseTest.class);
    @DisplayName("Неавторизованный пользователь покупает товар в 1клик")
    @Test
    public void oneClick() {
        mainPage.setSearchInput(propertiesManager.getProperty("productcode1"));
        cartPage.goToProductCard();
        productCardPage.clickToOneClick();
        checkOutPage.setInputSearchAddres("метро Фили");
        productCardPage.clickLupaButtonCard();
        new WebDriverWait(driver, 10).until(ExpectedConditions.invisibilityOf(driver.findElement(By.xpath("//*[@id='store-finder-map']"))));
        productCardPage.clickOneClickApteka(1);
        checkOutPage.setInputOneClickPhoneNumber(propertiesManager.getProperty("phonenumber"));
        checkOutPage.clickOnZabronirivatButton();
//        thankForTheOrder.checkOrderNumberSelf();
    }


}
