package web.makeorder.unauthorized;

import basetest.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

@Feature(value = "Web")
@Story(value = "Оформление заказа доставкой")
@DisplayName("Оформление заказа доставкой")
public class DeliveryOrderingTest extends BaseTest {
    private Logger logger = LogManager.getLogger(DeliveryOrderingTest.class);
    @DisplayName("Оформление заказа доставкой. Неавторизованный пользователь")
    @Test
    public void delivery() {
        mainPage.setSearchInput(propertiesManager.getProperty("productcode1"));
        cartPage.clickBuyButton();
        cartPage.clickToCartButton();
        cartPage.clickToOformitZakaz();
        checkOutPage.clickDeliveryMethod();
        checkOutPage.setInputAddress("Сиреневый бульвар 68");
        checkOutPage.clickToDropDownMenu();
        checkOutPage.setFlatNumber("12");
        checkOutPage.inputMyPhoneNumber("phoneNumber");
        checkOutPage.inputMyName(propertiesManager.getProperty("username"));
//        checkOutPage.inputMyEmail(propertiesManager.getProperty("usermail"));
        waitLoader();
        checkOutPage.clickToFinalButton();
        sberPage.inputCardNumber(propertiesManager.getProperty("cardnumber"));
        sberPage.inputMonthYear(propertiesManager.getProperty("monthyear"));
        sberPage.inputCvv(propertiesManager.getProperty("cvv"));
        sberPage.clickOnSubmitButton();
        thankForTheOrder.checkOrderNumberDelivery();
    }
}
