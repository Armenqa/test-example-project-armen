package web.makeorder.unauthorized;

import basetest.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@Feature(value = "Web")
@Story(value = "оформление заказа самовывозом")
@DisplayName("Оформление заказа самовывозом")
public class SelfPickupOrderingTest extends BaseTest {
    private Logger logger = LogManager.getLogger(SelfPickupOrderingTest.class);
    @DisplayName("Неавторизованный пользователь покупает товар со способом доставки - 'Самовывоз'")
    @Test
    public void pickup() {
        mainPage.setSearchInput(propertiesManager.getProperty("productcode1"));
        cartPage.clickBuyButton();
        cartPage.clickToCartButton();
        cartPage.clickToOformitZakaz();
        checkOutPage.setInputSearchAddres("метро Фили");
        checkOutPage.lupaButton();
        new WebDriverWait(driver, 10).until(ExpectedConditions.invisibilityOf(driver.findElement(By.xpath("//*[@id='store-finder-map']"))));
        checkOutPage.chengeAptekaList();
        checkOutPage.getAvailabilityAndChooseThisPharmacy();
        checkOutPage.inputMyPhoneNumber(propertiesManager.getProperty("phoneNumber"));
        checkOutPage.inputMyName("Армен");
//        checkOutPage.inputMyEmail(propertiesManager.getProperty("usermail"));
        checkOutPage.setOformitZakazButton();
        thankForTheOrder.checkOrderNumberSelf();

    }



}
