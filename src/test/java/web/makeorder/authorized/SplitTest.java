package web.makeorder.authorized;

import basetest.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

@Feature(value = "Web")
@Story("Оформление сплит заказа")
@DisplayName("Оформление сплит заказа")
public class SplitTest extends BaseTest {
    private Logger logger = LogManager.getLogger(SplitTest.class);
    @DisplayName("Авторизованный пользователь оформил заказ содержащий Партнерский товар + Не партнерский")
    @Test
    public void split() {
        cookiePage.cookieAuthorization();
        cartPage.clickToCartButton();
        cartPage.checkCartQuantity();
        mainPage.setSearchInput(propertiesManager.getProperty("productcode1"));
        cartPage.clickBuyButton();
        cartPage.clickToCartButton();
        mainPage.setSearchInput(propertiesManager.getProperty("productcode2"));
        cartPage.clickBuyButton();
        cartPage.clickToCartButton();
        cartPage.clickToOformitZakaz();
        checkOutPage.chengeAptekaList();
        checkOutPage.setInputSearchAddres("метро автозаводская");
        waitLoader();
        checkOutPage.getAvailabilityAndChooseThisPharmacy();
        waitLoader();
        checkOutPage.setOformitZakazButton();
        thankForTheOrder.checkOrderNumberSplit();
    }
}
