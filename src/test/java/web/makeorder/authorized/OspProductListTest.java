package web.makeorder.authorized;

import basetest.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import utils.PropertiesManager;

@Feature(value = "Web")
@Story("Пользователь покупает товар со страницы ОСП")
@DisplayName("Оформление товара со страниц ОСП")
public class OspProductListTest extends BaseTest {
    private Logger logger = LogManager.getLogger(OspProductListTest.class);
    private PropertiesManager propertiesManager = new PropertiesManager();
    @DisplayName("Авторизованный пользователь покупает товар со страниц ОСП")
    @Test
    public void checkOsp() {
        cookiePage.cookieAuthorization();
        cartPage.clickToCartButton();
        cartPage.checkCartQuantity();
        reloadPage();
        driver.get(propertiesManager.getProperty("baseurl"));
        logger.info("ПОЛЬЗОВАТЕЛЬ ПЕРЕШЕЛ НА ГЛАВНУЮ СТРАНИЦУ САЙТА");
        mainPage.ClickOnLetter();
        ospPage.ClickOnProductName();
        ospPage.clickOnKupitButton(1);
        cartPage.clickToCartButton();
        cartPage.clickToOformitZakaz();
        checkOutPage.setInputSearchAddres("метро автозаводская");
        checkOutPage.chengeAptekaList();
        checkOutPage.getAvailabilityAndChooseThisPharmacy();
        waitLoader();
        checkOutPage.setOformitZakazButton();
        thankForTheOrder.checkOrderNumberSelf();
    }
}
