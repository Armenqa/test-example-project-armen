package web.makeorder.authorized;

import basetest.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@Feature(value = "Web")
@Story("Покупка дисконтного товар")
@DisplayName("Оформление дисконтного товара в 1клик")
public class DiscountOrderingTest extends BaseTest {
    private Logger logger = LogManager.getLogger(DiscountOrderingTest.class);
    @DisplayName("Авторизованный пользователь покупает товар по дисконтной цене в 1клик")
    @Test
    public void discount() {
        cookiePage.cookieAuthorization();
        cartPage.clickToCartButton();
        cartPage.checkCartQuantity();
        reloadPage();
        openUrl(propertiesManager.getProperty("baseurl") + "p/33090");
        productCardPage.clickDiscountPrice();
        waitLoader();
        productCardPage.clickToOneClick();
        checkOutPage.setInputSearchAddres("метро Фили");
        productCardPage.clickLupaButtonCard();
        new WebDriverWait(driver, 10).until(ExpectedConditions.invisibilityOf(driver.findElement(By.xpath("//*[@id='store-finder-map']"))));
        productCardPage.changeAptekaListDiscount();
        productCardPage.clickOneClickApteka(0);
        checkOutPage.setInputOneClickPhoneNumber(propertiesManager.getProperty("phonenumber"));
        checkOutPage.clickOnZabronirivatButton();
//        thankForTheOrder.checkOrderNumberSelf();

    }

}
