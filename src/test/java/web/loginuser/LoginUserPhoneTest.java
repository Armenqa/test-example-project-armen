package web.loginuser;

import basetest.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import utils.PropertiesManager;

@Feature(value = "Web")
@Story(value = "Авторизация пользователя")
@DisplayName("Авторизация пользователя по номеру телефона")
public class LoginUserPhoneTest extends BaseTest {
    PropertiesManager propertiesManager = new PropertiesManager();
    private Logger logger = LogManager.getLogger(LoginUserPhoneTest.class);
    @DisplayName("Пользователь авторизовывается на сайте по номеру телефона")
    @Test
    public void loginUserPhone() {
        cookiePage.reCaptchaKey();
        topPanel.clickToLoginIcon();
        authPopUp.clickChangeAuth();
        authPopUp.setPhoneNumber(propertiesManager.getProperty("userauthphone"));
        authPopUp.setPassword(propertiesManager.getProperty("userpass"));
        authPopUp.clickToLoginButton();
        mainPage.checkElementIsCorrect();
        waitLoader();
        returnAttachment();
    }

}
