package web.loginuser;

import basetest.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.junit.Test;
import utils.PropertiesManager;

@Feature(value = "Web")
@Story("Авторизация пользователя")
@DisplayName("Авторизация пользователя по номеру Бонусной карты")
public class LoginUserCardTest extends BaseTest {
    PropertiesManager propertiesManager = new PropertiesManager();
    @DisplayName("Пользователь авторизовывается на сайте по номеру Бонусной карты")
    @Test
    public void loginUserCardNumber() {
        cookiePage.reCaptchaKey();
        topPanel.clickToLoginIcon();
        authPopUp.setChangeToCardNumber();
        authPopUp.inputAuthCardNumber(propertiesManager.getProperty("userauthcard"));
        authPopUp.setPassword(propertiesManager.getProperty("userpass"));
        authPopUp.clickToLoginButton();
        mainPage.checkElementIsCorrect();
    }

}
