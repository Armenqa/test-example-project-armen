package web.loginuser;

import basetest.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.junit.Test;

@Feature(value = "Web")
@Story("Авторизация пользователя")
@DisplayName("Авторизация пользователя по E-mail")
public class LoginUserEmailTest extends BaseTest {
    @DisplayName("Пользователь авторизовывается на сайте через E-mail")
    @Test
    public void loginUserEmail() {
        cookiePage.reCaptchaKey();
        topPanel.clickToLoginIcon();
        authPopUp.changeToEmailAuth();
        authPopUp.inputEmail(propertiesManager.getProperty("userauthmail"));
        authPopUp.setPassword(propertiesManager.getProperty("userpass"));
        authPopUp.clickToLoginButton();
        waitLoader();
        mainPage.checkElementIsCorrect();
    }

}
