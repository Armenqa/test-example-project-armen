package mobile;

import basetest.BaseMobileTest;
import basetest.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

@Feature(value = "Мобильная версия")
@Story("Оформление сплит заказа. Мобильная версия")
@DisplayName("Оформление сплит заказа. Мобильная версия")
public class SplitTest extends BaseMobileTest {
    private Logger logger = LogManager.getLogger(SplitTest.class);
    @DisplayName("Неавторизованный пользователь оформляет заказ содержащий Партнерский товар + Не партнерский")
    @Test
    public void split() {
        mobileMainPage.setSearchInput(propertiesManager.getProperty("productcode1"));
        mobileCartPage.clickBuyButton();
        mobileCartPage.clickToCartButton();
        openUrl(propertiesManager.getProperty("baseurl") + "p/35052");
        waitLoader();
        mobileCartPage.clickVkorzinuButton();
        mobileCartPage.clickToCartButton();
        mobileCartPage.clickToOformitZakaz();
        mobileCheckOutPage.setInputSearchAddres("метро автозаводская");
        mobileCheckOutPage.chengeAptekaList();
        mobileCheckOutPage.getAvailabilityAndChooseThisPharmacy();
        mobileCheckOutPage.inputMyPhoneNumber(propertiesManager.getProperty("phoneNumber"));
        mobileCheckOutPage.inputMyName(propertiesManager.getProperty("username"));
        mobileCheckOutPage.inputMyEmail(propertiesManager.getProperty("usermail"));
        mobileCheckOutPage.clickMyEmail();
        mobileCheckOutPage.setOformitZakazButton();
        mobileThankForTheOrderPage.checkOrderNumberSplit();
        mobileThankForTheOrderPage.ScrollToObject();
    }
}
