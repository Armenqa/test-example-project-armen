package mobile;

import basetest.BaseMobileTest;
import basetest.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@Feature(value = "Мобильная версия")
@Story(value = "Оформление основного товара в 1 клик. Мобильная версия")
@DisplayName("Оформление основного товара в 1 клик. Мобильная версия")
public class OneClickPurchaseTest extends BaseMobileTest {
    private Logger logger = LogManager.getLogger(OneClickPurchaseTest.class);
    @DisplayName("Неавторизованный пользователь покупает товар в 1клик")
    @Test
    public void oneClick() {
        mobileCookiePage.reCaptchaKey();
        openUrl(propertiesManager.getProperty("baseurl") + "p/61819");
        mobileProductCardPage.clickToOneClick();
        mobileProductCardPage.scrollToProductStores();
        mobileCheckOutPage.setInputSearchAddres("метро Фили");
        mobileProductCardPage.clickLupaButtonCard();
        new WebDriverWait(driver, 10).until(ExpectedConditions.invisibilityOf(driver.findElement(By.xpath("//*[@id='store-finder-map']"))));
        mobileProductCardPage.clickList();
        mobileProductCardPage.scrollDown();
        mobileProductCardPage.clickOneClickApteka(0);
        mobileCheckOutPage.setInputOneClickPhoneNumber(propertiesManager.getProperty("phonenumber"));
        mobileCheckOutPage.clickOnZabronirivatButton();
        mobileThankForTheOrderPage.checkOrderNumberSelf();
    }


}
