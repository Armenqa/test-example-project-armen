package mobile;

import basetest.BaseMobileTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

@Feature(value = "Мобильная версия")
@Story(value = "Оформление заказа доставкой. Мобильная версия")
@DisplayName("Оформление заказа доставкой. Мобильная версия")
public class DeliveryOrderingTest extends BaseMobileTest {
    private Logger logger = LogManager.getLogger(web.makeorder.unauthorized.DeliveryOrderingTest.class);
    @DisplayName("Оформление заказа доставкой. Неавторизованный пользователь")
    @Test
    public void delivery() {
        mobileMainPage.setSearchInput(propertiesManager.getProperty("productcode1"));
        mobileCartPage.clickBuyButton();
        mobileCartPage.clickToCartButton();
        waitLoader();
        mobileCartPage.clickToOformitZakaz();
        waitLoader();
        mobileCheckOutPage.clickDeliveryMethod();
        mobileCheckOutPage.setInputAddress("Автозаводская 11");
        mobileCheckOutPage.clickToDropDownMenu();
        mobileCheckOutPage.setSpecifyDeliveryData();
        mobileCheckOutPage.setFlatNumber("12");
        mobileCheckOutPage.inputMyPhoneNumber("phoneNumber");
        mobileCheckOutPage.inputMyName(propertiesManager.getProperty("username"));
        waitLoader();
        mobileCheckOutPage.clickToFinalButton();
        logger.info("ПОЛЬЗОВАТЕЛЬ ПЕРЕШЕЛ НА СТРАНИЦУ СБЕРА");
        mobileSberPage.inputCardNumber(propertiesManager.getProperty("cardnumber"));
        mobileSberPage.inputMonthYear(propertiesManager.getProperty("monthyear"));
        mobileSberPage.inputCvv(propertiesManager.getProperty("cvv"));
        mobileSberPage.clickOnSubmitButton();
        mobileThankForTheOrderPage.checkOrderNumberDelivery();

    }
}
