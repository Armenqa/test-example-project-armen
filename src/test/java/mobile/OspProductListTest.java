package mobile;

import basetest.BaseMobileTest;
import basetest.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import utils.PropertiesManager;

@Feature(value = "Мобильная версия")
@Story("Пользователь покупает товар со страницы ОСП. Мобильная версия")
@DisplayName("Оформление товара со страниц ОСП. Мобильная версия")
public class OspProductListTest extends BaseMobileTest {
    private Logger logger = LogManager.getLogger(OspProductListTest.class);
    private PropertiesManager propertiesManager = new PropertiesManager();
    @DisplayName("Неавторизованный пользователь покупает товар со страниц ОСП")
    @Test
    public void checkOsp() {
        mobileMainPage.ClickOnLetter();
        mobileOspPage.clickToShawAll();
        mobileOspPage.ClickOnProductName();
        mobileOspPage.ScrollToObject();
        mobileOspPage.clickFirst();
        waitLoader();
        mobileCartPage.clickToCartButton();
        mobileCartPage.clickToOformitZakaz();
        mobileCheckOutPage.setInputSearchAddres("метро автозаводская");
        mobileCheckOutPage.chengeAptekaList();
        mobileCheckOutPage.getAvailabilityAndChooseThisPharmacy();
        mobileCheckOutPage.inputMyPhoneNumber(propertiesManager.getProperty("phoneNumber"));
        mobileCheckOutPage.inputMyName(propertiesManager.getProperty("username"));
        mobileCheckOutPage.inputMyEmail(propertiesManager.getProperty("usermail"));
        mobileCheckOutPage.clickMyEmail();
        mobileCheckOutPage.setOformitZakazButton();
        mobileThankForTheOrderPage.checkOrderNumberSelf();


    }
}
