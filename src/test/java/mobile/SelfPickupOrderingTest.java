package mobile;

import basetest.BaseMobileTest;
import basetest.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@Feature(value = "Мобильная версия")
@Story(value = "оформление заказа самовывозом. Мобильная версия")
@DisplayName("Оформление заказа самовывозом. Мобильная версия")
public class SelfPickupOrderingTest extends BaseMobileTest {
    private Logger logger = LogManager.getLogger(SelfPickupOrderingTest.class);
    @DisplayName("Неавторизованный пользователь покупает товар со способом доставки - 'Самовывоз'")
    @Test
    public void pickup() {
        mobileMainPage.setSearchInput(propertiesManager.getProperty("productcode1"));
        mobileCartPage.clickBuyButton();
        mobileCartPage.clickToCartButton();
        mobileCartPage.clickToOformitZakaz();
        mobileCheckOutPage.setInputSearchAddres("метро Фили");
        mobileCheckOutPage.ScrollToObject();
        mobileCheckOutPage.lupaButton();
        new WebDriverWait(driver, 10).until(ExpectedConditions.invisibilityOf(driver.findElement(By.xpath("//*[@id='store-finder-map']"))));
        mobileCheckOutPage.chengeAptekaList();
        mobileCheckOutPage.getAvailabilityAndChooseThisPharmacy();
        mobileCheckOutPage.inputMyPhoneNumber(propertiesManager.getProperty("phoneNumber"));
        mobileCheckOutPage.inputMyName("Армен");
//        mobileCheckOutPage.inputMyEmail(propertiesManager.getProperty("usermail"));
//        mobileCheckOutPage.clickMyEmail();
        mobileCheckOutPage.setOformitZakazButton();
        mobileThankForTheOrderPage.checkOrderNumberSelf();

    }



}
