package mobile;

import basetest.BaseMobileTest;
import basetest.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@Feature(value = "Мобильная версия")
@Story("Покупка дисконтного товар. Мобильная версия")
@DisplayName("Оформление дисконтного товара в 1клик. Мобильная версия")
public class DiscountOrderingTest extends BaseMobileTest {
    private Logger logger = LogManager.getLogger(DiscountOrderingTest.class);
    @DisplayName("Неавторизованный пользователь покупает товар по дисконтной цене в 1клик")
    @Test
    public void discount() {
        mobileCookiePage.reCaptchaKey();
        openUrl(propertiesManager.getProperty("baseurl") + "p/33090");
        mobileProductCardPage.scrollToProfitableOffer();
        mobileProductCardPage.clickDiscountPrice();
        waitLoader();
        mobileProductCardPage.scrollToProductStores();
        mobileProductCardPage.clickToOneClick();
        mobileCheckOutPage.setInputSearchAddres("метро Автозаводская");
        mobileProductCardPage.clickLupaButtonCard();
        new WebDriverWait(driver, 10).until(ExpectedConditions.invisibilityOf(driver.findElement(By.xpath("//*[@id='store-finder-map']"))));
        mobileProductCardPage.clickList();
        mobileProductCardPage.clickOneClickApteka(0);
        mobileCheckOutPage.setInputOneClickPhoneNumber(propertiesManager.getProperty("phonenumber"));
        mobileCheckOutPage.clickOnZabronirivatButton();
//        mobileThankForTheOrderPage.checkOrderNumberSelf();

    }

}
