package api;

import httpsteps.ApiSteps;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class JSONAssertTest {
    private ApiSteps apiSteps = new ApiSteps();

    @Test
    public void jsonAssertTest() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("fields", "FULL");
        apiSteps.get("gorzdrav/77-gz/cart", params);
        apiSteps.assertResponse("src/test/resources/jsonfiles/responses/empty-cart.json");
    }
}
